//
//  MyTabsController.swift
//  Application
//
//  Created by Cristhian Motoche on 2/6/17.
//  Copyright © 2017 Cristhian Motoche. All rights reserved.
//

import UIKit

class MyTabsController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let items = self.tabBar.items

        items?[0].title = "Sign Up!"

        items?[1].title = "View User Log"
        items?[1].badgeValue = "1"
    }
}
