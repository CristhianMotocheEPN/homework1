//
//  UsersTableViewController.swift
//  Application
//
//  Created by Cristhian Motoche on 2/6/17.
//  Copyright © 2017 Cristhian Motoche. All rights reserved.
//

import UIKit

class UsersTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var userList:[Any] = []
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        let clientAPI = ClientAPI("http://localhost:1337/user")
        clientAPI.getData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
}
