//
//  User.swift
//  Application
//
//  Created by Cristhian Motoche on 15/5/17.
//  Copyright © 2017 Cristhian Motoche. All rights reserved.
//

import Foundation

class User {
    var userName:String = ""
    var userParssword:String = ""
    var userEmail:String = ""

    init(name: String, password:String, email:String) {
        self.userName = name
        self.userParssword = password
        self.userEmail = email
    }
    
    func toDictionary() -> [String:String] {
        return ["username":self.userName,"password":self.userParssword,"email":self.userEmail]
    }
}
