//
//  UserTableViewController.swift
//  Application
//
//  Created by Cristhian Motoche on 2/6/17.
//  Copyright © 2017 Cristhian Motoche. All rights reserved.
//

import UIKit

class UserTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var usersArray: NSArray = []
    @IBOutlet weak var tableView: UITableView!
    override func viewWillAppear(_ animated: Bool) {
        let clientApi = ClientAPI("http://localhost:1337/user")
        clientApi.getData()
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarDatos), name: NSNotification.Name("actualizarUsuarios"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let username = (usersArray[indexPath.row] as! NSDictionary)["username"] as! String
        cell.textLabel?.text = "Username: \(username)"
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Usuarios"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func actualizarDatos(_ notification:Notification){
        guard let usersResponse = notification.userInfo?["users"] as? NSArray else {
            print("error")
            return
        }
        self.usersArray = usersResponse
        self.tableView.reloadData()
    }
}
