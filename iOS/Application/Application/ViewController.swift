//
//  ViewController.swift
//  Application
//
//  Created by Cristhian Motoche on 15/5/17.
//  Copyright © 2017 Cristhian Motoche. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var pwd: UITextField!
    
    
    @IBOutlet weak var email: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    @IBAction func sendRequest(_ sender: Any) {
        let user = User(name: (username?.text)!, password: (pwd?.text)!, email: (username?.text)!)
        let clientApi = ClientAPI("http://localhost:1337/user")
        clientApi.sendRequest(userForm: user)
        clean()
        alert(msg: "Request send!")
    }
    
    func clean() -> Void {
        self.username?.text = ""
        self.pwd?.text = ""
        self.email?.text = ""
    }
    
    func alert(msg: String) -> Void {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
