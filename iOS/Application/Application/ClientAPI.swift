//
//  Controller.swift
//  Application
//
//  Created by Cristhian Motoche on 15/5/17.
//  Copyright © 2017 Cristhian Motoche. All rights reserved.
//

import Foundation
import Alamofire

class ClientAPI {
    var url:String = ""
    
    init(_ url: String) {
        self.url = url
    }
    
    func sendRequest(userForm: User){
        Alamofire.request(self.url, method: .post, parameters: userForm.toDictionary(), encoding: JSONEncoding.default).responseJSON { response in
            if let data = response.data {
                let json = String(data: data, encoding: String.Encoding.utf8)
                print("User Created: \(json!)")
            }
        }
    }
    
    func getData() -> Void {
        Alamofire.request(url).responseJSON { response in
            if let JSON = response.result.value {
                let jsonUsers = JSON as! NSArray
                
                NotificationCenter.default.post(name: NSNotification.Name("actualizarUsuarios"), object: nil, userInfo: ["users":jsonUsers])
            }
        }
    }
}
