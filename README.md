# Homework

## Start server
To start the server run the following command:

```shell
$ cd Sails/server
$ sails lift
```

## Database

Run the following command with [docker](https://docs.docker.com/engine/installation/done):

```shell
$ docker pull postgres
$ docker run -d --name db -d -e POSTGRES_PASSWORD=123456 -e POSTGRES_USER=usuario -e POSTGRES_DB=base -p 5432:5432 -d postgres:latest
```
